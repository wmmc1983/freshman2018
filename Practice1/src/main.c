/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"


void port_init(void) {
	//
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	//
	GPIOB->MODER &= ~GPIO_MODER_MODER0_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER0_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER1_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER1_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER6_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER6_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER7_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER7_Pos);
}


int main(void) {
	//
	port_init();

	//
	GPIOB->ODR |= 1 << 0;
	GPIOB->ODR |= 1 << 1;
	GPIOB->ODR |= 1 << 6;
	GPIOB->ODR |= 1 << 7;

	for(;;) {
	}
	return 0;
}
