/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"


void port_init(void) {
	//
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	//
	GPIOB->MODER &= ~GPIO_MODER_MODER0_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER0_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER1_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER1_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER6_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER6_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER7_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER7_Pos);

	//
	GPIOA->MODER &= ~GPIO_MODER_MODER11_Msk;
	GPIOA->MODER |= (GPIO_MODE_INPUT << GPIO_MODER_MODER11_Pos);
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR11_Msk;
	GPIOA->PUPDR |= (GPIO_PULLUP << GPIO_PUPDR_PUPDR11_Pos);

	GPIOA->MODER &= ~GPIO_MODER_MODER12_Msk;
	GPIOA->MODER |= (GPIO_MODE_INPUT << GPIO_MODER_MODER12_Pos);
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR12_Msk;
	GPIOA->PUPDR |= (GPIO_PULLUP << GPIO_PUPDR_PUPDR12_Pos);

	GPIOA->MODER &= ~GPIO_MODER_MODER15_Msk;
	GPIOA->MODER |= (GPIO_MODE_INPUT << GPIO_MODER_MODER15_Pos);
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR15_Msk;
	GPIOA->PUPDR |= (GPIO_PULLUP << GPIO_PUPDR_PUPDR15_Pos);
}


void sysclk_init(void) {
	RCC->CFGR |= RCC_CFGR_PLLSRC_HSI_DIV2; // PLL <== HSI/2 = 4MHz
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV2; // APB1 = PLL / 2 = 32MHz
	RCC->CFGR |= ( (16 - 1) << RCC_CFGR_PLLMUL_Pos); // x16
	FLASH->ACR |= FLASH_ACR_LATENCY_1; // flash access latency for 48 < HCLK <= 72. This statement must be placed immediately after PLL multiplication.
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY)); // wait until PLL is ready
	RCC->CFGR |= RCC_CFGR_SW_PLL; // PLL as system clock

	while( (RCC->CFGR & RCC_CFGR_SWS_Msk) != RCC_CFGR_SWS_PLL );
	SystemCoreClockUpdate();
}


void ms_wait(uint32_t ms) {
	SysTick->LOAD = 8000 - 1; // sysclk = 64MHz, prescaled by 8
	SysTick->VAL = 0; // reset count value
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; // count start

	for(uint32_t i = 0; i < ms; i++) {
		while( !(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) ); // wait for 1ms count
	}
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk; // count stop
}


int main(void) {
	//
	sysclk_init();
	port_init();

	for(;;) {
		if( !(GPIOA->IDR & (1 << 11)) ) {
			GPIOB->ODR |= GPIO_ODR_0;
		}else{
			GPIOB->ODR &= ~GPIO_ODR_0;
		}

		if( !(GPIOA->IDR & (1 << 12)) ) {
			GPIOB->ODR |= GPIO_ODR_1;
		}else{
			GPIOB->ODR &= ~GPIO_ODR_1;
		}

		if( !(GPIOA->IDR & (1 << 15)) ) {
			GPIOB->ODR |= GPIO_ODR_6;
		}else{
			GPIOB->ODR &= ~GPIO_ODR_6;
		}
	}
	return 0;
}
